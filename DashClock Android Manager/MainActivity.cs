﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Database;
using Android.Views;
using Java.Lang;
using System;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DashClock_Android_Manager
{
    [Activity(Label = "DashClock_Android_Manager", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        ListView alarmsListView;
        EditText editAlarmUrl;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView (Resource.Layout.Main);
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetActionBar(toolbar);
            ActionBar.Title = "DashClock Manager";

            Button button = FindViewById<Button>(Resource.Id.testButton1);
            alarmsListView = FindViewById<ListView>(Resource.Id.alarmsListView1);
            editAlarmUrl = FindViewById<EditText>(Resource.Id.editAlarmUrl1);

            button.Click += Button_Click;
        }

        private async void Button_Click(object sender, System.EventArgs e)
        {
            alarmsListView.Adapter = new AlarmAdapter(this, await getAlarmsFromApi(editAlarmUrl.Text));
        }

        private async Task<IList<Alarm>> getAlarmsFromApi(string baseUrl)
        {
            HttpClient client = new HttpClient();
            var headers = client.DefaultRequestHeaders;
            Uri requestUri = new Uri(baseUrl);
            HttpResponseMessage msg = new HttpResponseMessage();
            string body = "";
            IList<Alarm> alarms = new List<Alarm>();

            try
            {
                msg = await client.GetAsync(requestUri);
                msg.EnsureSuccessStatusCode();
                body = await msg.Content.ReadAsStringAsync();

                alarms = JsonConvert.DeserializeObject<IList<Alarm>>(body);
            }
            catch (System.Exception)
            {
                //PASS
            }
            return alarms;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.top_main_menu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if(item.ItemId == Resource.Id.menu_preferences)
            {
                //TODO: Open Overflow menu
            }
            return base.OnOptionsItemSelected(item);
        }
    }

    public class AlarmAdapter : BaseAdapter<Alarm>
    {
        public override Alarm this[int position] => alarms[position];
        public override int Count => alarms.Count;

        private IList<Alarm> alarms;
        private Activity context;

        public AlarmAdapter(Activity context, IList<Alarm> alarms) : base()
        {
            this.context = context;
            this.alarms = alarms;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = this[position];
            View view = convertView;
            if(view == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.listItemLayout, null);
            }
            
            string timeText = new DateTime(1, 1, 1, item.hour, item.minute, 0).ToString("h:mm tt");
            string daysText = item.ToDaysString();
            view.FindViewById<TextView>(Resource.Id.AlarmTime1).Text = timeText;
            view.FindViewById<TextView>(Resource.Id.AlarmDays1).Text = daysText;
            return view;
        }
    }
}

