﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace DashClock_Android_Manager
{
    public class Alarm
    {
        public int id;
        public string guid;
        public IList<string> daysOfWeek;
        public bool everyDay;
        public int hour;
        public int minute;

        public string ToJSON()
        {
            return JsonConvert.SerializeObject(this);
        }

        public string ToDaysString()
        {
            if(!everyDay)
            {
                string retStr = "";
                for (int i = 0; i < daysOfWeek.Count; i++)
                {
                    if (i == 0)
                    {
                        retStr = retStr + daysOfWeek[i];
                    }
                    else
                    {
                        retStr = retStr + ", " + daysOfWeek[i];
                    }
                }
                return retStr;
            }
            else
            {
                return "Every day";
            }
        }
    }
}